//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import logo from '../../../../img/logo.png';
import colorApp from '../../../ultis/styles/common-css';
import Header from '../../../components/header/Header';
import Communications from 'react-native-communications';
// create a component
export default class ForgotPassword extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    title={'Tìm mật khẩu'}
                    navigation={this.props.navigation}
                />
                <Image
                    source={logo}
                    style={styles.imageLogo}
                    resizeMode={'contain'}
                />
                <View style={styles.preTextInput}>
                    <Text style={[styles.textTitle, { fontSize: 16, color: 'black' }]}>Thông tin chi tiết liên hệ hotline: </Text>
                    <TouchableOpacity
                        onPress={() => Communications.phonecall('1900 63 64 77', true)}
                    >
                        <Text style={[styles.textTitle, { fontSize: 16, marginLeft: 0, textDecorationLine: 'underline' }]}>1900 63 64 77</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },
    btnLogin: {
        width: 150,
        height: 50,
        alignSelf: 'center',
        borderRadius: 8,
        backgroundColor: '#4493AA',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    textStyle: {
        fontSize: 16,
    },
    imageLogo: {
        width: '70%',
        height: '15%',
        alignSelf: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    textTitle: {
        fontSize: 20,
        color: colorApp.colorBlue,
        marginTop: 10,
        marginLeft: 15
    },
    preTextInput: {
        flexDirection: 'row'
    },
    textInput: {
        borderWidth: 0.5,
        borderRadius: 5,
        marginBottom: 10,
    },
    textLabel: {
        color: '#bfbfbf',
        fontWeight: 'normal'
    },
    iconColor: '#4493AA',
};

//make this component available to the app
