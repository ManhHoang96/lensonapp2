//import liraries
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, Keyboard } from 'react-native';
import HeaderComponent from '../../../../components/header/Header';
import colorApp from '../../../../ultis/styles/common-css';
import AccountInfoController from './AccountInfoController';
import { NavigationActions } from 'react-navigation';
import TextInput from '../../../../components/custom-textinput/CustomTextInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
// create a component
class AccountInfo extends Component {
    constructor(props) {
        super(props);
        accountInfo = new AccountInfoController();
        this.state = {
            isLoading: false,
        }
    }
    handleFocus(nextInput) {
        this.refs[nextInput].text.focus();
    }

    render() {
        return (
            <KeyboardAwareScrollView
                style={styles.container}
            >
                <HeaderComponent
                    navigation={this.props.navigation}
                    title={'Thông tin tài khoản'}
                    onPressBack={() => {
                        let resetAction = NavigationActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'Login' })
                            ]
                        })
                        this.props.navigation.dispatch(resetAction)
                    }}
                />
                <View style={styles.preTextInput}>
                    <TextInput
                        label={'Tài khoản'}
                        onChangeText={(e) => accountInfo.setAccount(e)}
                        onSubmitEditing={(text) => { this.handleFocus(1) }}
                        returnKeyType={'next'}
                        note={'Không có kí tự đặc biệt'}
                        noteStyle={styles.noteStyle}
                        autoFocus
                    />
                    <TextInput
                        label={'Email'}
                        onChangeText={(e) => accountInfo.setEmail(e)}
                        note={'Ví dụ: abc123@gmail.com'}
                        onSubmitEditing={() => this.handleFocus(2)}
                        returnKeyType={'next'}
                        ref='1'
                    />
                    <TextInput
                        label={'Mật khẩu'}
                        note={'Nên đặt ít nhất 6 kí tự'}
                        onChangeText={(e) => accountInfo.setPassword(e)}
                        onSubmitEditing={() => this.handleFocus(3)}
                        returnKeyType={'next'}
                        secureTextEntry
                        ref='2'
                    />
                    <TextInput
                        label={'Nhập lại mật khẩu'}
                        onChangeText={(e) => accountInfo.setRetypePassword(e)}
                        secureTextEntry
                        ref='3'
                    />
                </View>

                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss();
                        accountInfo.checkValueAccount(this.props.navigation, this)
                    }}
                    style={styles.btnLogin}
                >
                    <Text style={[styles.textStyle]}>Tiếp theo</Text>
                </TouchableOpacity>
                {
                    (this.state.isLoading === true) ? <ActivityIndicator /> : null
                }

            </KeyboardAwareScrollView>
        );
    }
}

// define your styles
const styles = {
    container: {
        flex: 1,
    },
    btnLogin: {
        width: 200,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 50,
        borderRadius: 5,
        backgroundColor: colorApp.colorBlue
    },
    textStyle: {
        color: 'white',
        fontSize: 16
    },

    textTitle: {
        fontSize: 16,
        marginTop: 10,
        marginBottom: 20,
        color: 'black',
        marginLeft: 5
    },

    textInput: {
        borderWidth: 0.5,
        borderRadius: 5,
        marginBottom: 10,
    },
    textLabel: {
        color: '#bfbfbf',
        fontWeight: 'normal'
    },
    iconColor: '#4493AA',
    preTextInput: {
        width: '100%',
        height: 410,
        paddingLeft: 15,
        paddingRight: 15,
        justifyContent: 'space-between',
        marginTop: 10
    },
}
const mapStateToProps = (state, ownProps) => {
    return {
        isOnline: state.getOnline.data,
    }
}
export default connect(mapStateToProps)(AccountInfo);

//make this component available to the app
