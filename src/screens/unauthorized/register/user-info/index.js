import React, { Component } from 'react';
import { View, TouchableOpacity, Dimensions, Text, ActivityIndicator, AsyncStorage } from 'react-native';
// Import default color in app
import colorApp from '../../../../ultis/styles/common-css';
import { Toast } from 'native-base';
import { NavigationActions } from 'react-navigation';
//import libs
import HeaderComponent from '../../../../components/header/Header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
//import dataLocation
import data from '../../../../ultis/api/local-data/province.json';
import Picker from '../../../../components/custom-picker';
import TextInput from '../../../../components/custom-textinput/CustomTextInput';
import { getCurrentUser } from '../../../../redux/actions/get-current-user';
import { connect } from 'react-redux';
const screen = Dimensions.get('window');
const uuidv1 = require('uuid/v1');
class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            data: data[0].branch
        }
        fullName = '';
        phoneNum = '';
        addressCity = 1;
        bankNum = '';
        bankName = 1;
        accountHolder = '';
        isDisable = false;
    }
    handleFocus(nextInput) {
        this.refs[nextInput].text.focus();
    }
    checkInvalidMobile(mobile) {
        if (!(mobile.match("[0-9]+"))) return false;
        if (mobile.startsWith("09") && mobile.length == 10) return true;
        if (mobile.startsWith("01") && mobile.length == 11) return true;
        if (mobile.startsWith("849") && mobile.length == 11) return true;
        if (mobile.startsWith("841") && mobile.length == 12) return true;
        return false;
    }
    showToast(text) {
        Toast.show({
            text: text,
            position: 'top',
            buttonText: 'Okay',
            duration: 2000
        })
    }
    setSessionId = async (uuid, responseJson) => {
        try {
            await AsyncStorage.setItem("key", uuid);
            if (responseJson.data.loginCode === 0) {
                Toast.show({
                    text: 'Đăng ký thành công!',
                    position: 'top',
                    buttonText: 'Đăng nhập',
                    type: 'success',
                    onClose: () => {
                        this.props.getCurrentUser(responseJson.data);
                        const resetAction = NavigationActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({
                                    routeName: 'Home',
                                })
                            ]
                        })
                        this.props.navigation.dispatch(resetAction)
                    }
                })

            }
        } catch (error) {

        }
    }
    signup() {
        if (fullName === '') {
            this.showToast('Họ và tên trống!')
            return;
        }
        if (phoneNum === '') {
            this.showToast('Số điện thoại trống!')
            return;
        }
        if (this.checkInvalidMobile(phoneNum) === false) {
            this.showToast('Số điện thoại không hợp lệ!')
            return;
        }
        if (accountHolder === '') {
            this.showToast('Chủ tài khoản thẻ trống!')
            return;
        }
        if (bankNum === '') {
            this.showToast('Số tài khoản trống!')
            return;
        }
        if (this.props.isOnline === true && isDisable === false) {
            isDisable = true;
            this.setState({
                isLoading: true
            })
            let sessionId = uuidv1();
            fetch('https://app-dot-casper-electric.appspot.com/api?action=signUp', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    account: this.props.navigation.state.params.account,
                    password: this.props.navigation.state.params.password,
                    email: this.props.navigation.state.params.email,
                    phoneNum: phoneNum,
                    addressCity: addressCity,
                    bankName: bankName,
                    bankNum: bankNum,
                    accountHolder: accountHolder,
                    fullName: fullName,
                    sessionId: sessionId
                })
            })
                .then((response) => response.json())
                .then(async (responseJson) => {
                    this.setState({ isLoading: false });
                    //this.setSessionId(sessionId, responseJson);
                    try {
                        await AsyncStorage.setItem("key", sessionId);
                        if (responseJson.data.loginCode === 0) {
                            Toast.show({
                                text: 'Đăng ký thành công!',
                                position: 'top',
                                buttonText: 'Đăng nhập',
                                type: 'success',
                                onClose: () => {
                                    //this.props.getCurrentUser(responseJson.data);
                                    const resetAction = NavigationActions.reset({
                                        index: 0,
                                        actions: [
                                            NavigationActions.navigate({
                                                routeName: 'Login',
                                            })
                                        ]
                                    })
                                    this.props.navigation.dispatch(resetAction)
                                }
                            })

                        }
                    } catch (error) {

                    }

                })
                .catch((error) => {

                })
        } else {
            this.showToast('Không có kết nối internet!')
            isDisable = false;
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderComponent
                    navigation={this.props.navigation}
                    title={'Thông tin người dùng'}
                />
                <KeyboardAwareScrollView
                    style={styles.container}
                >
                    <View style={styles.preTextInput}>
                        <TextInput
                            label={'Họ và tên'}
                            onChangeText={(e) => fullName = e}
                            onSubmitEditing={() => this.handleFocus(1)}
                            note={'Ví dụ: Nguyễn Văn An'}
                        />
                        <TextInput
                            label={'Số điện thoại'}
                            onChangeText={(e) => phoneNum = e}
                            typePhone
                            ref='1'
                        />
                        <Text style={[styles.textTitle, { alignSelf: 'center', marginTop: 20 }]}>
                            Ngân hàng VietinBank
                    </Text>

                        <Picker
                            getAddressCity={(value) => {
                                addressCity = value;
                                this.setState({ data: data[value - 1].branch })
                            }}
                            data={data}
                            style={styles.picker}
                            placeholder={'Chọn tỉnh/thành phố'}
                            iosHeader={'Chọn tỉnh/thành phố'}
                        />

                        <TextInput
                            label={'Chi nhánh ngân hàng'}
                            onChangeText={(e) => bankName = e}
                            onSubmitEditing={() => { this.handleFocus(2) }}
                            note={'Ví dụ: Vietinbank Ba Đình'}
                        />
                        <TextInput
                            label={'Chủ tài khoản thẻ'}
                            onChangeText={(e) => accountHolder = e}
                            onSubmitEditing={() => this.handleFocus(3)}
                            ref='2'
                            note={'Ví dụ: NGUYEN VAN AN'}
                        />
                        <TextInput
                            label={'Số tài khoản'}
                            onChangeText={(e) => bankNum = e}
                            ref='3'
                            note={'Ví dụ: 711AA654321'}
                        />

                    </View>
                    {
                        (this.state.isLoading === true) ? <ActivityIndicator /> : null
                    }
                    <TouchableOpacity
                        style={styles.btnLogin}
                        onPress={() => this.signup()}
                    >
                        <Text style={{ color: 'white', fontSize: 16 }}>Đăng ký</Text>
                    </TouchableOpacity>

                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },
    btnLogin: {
        width: 200,
        height: 50,
        alignSelf: 'center',
        borderRadius: 5,
        backgroundColor: '#4493AA',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    textInput: {
        borderWidth: 0.5,
        borderRadius: 5,
        marginBottom: 10,
    },

    textStyle: {
        color: colorApp.defaultTextColor,
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        fontSize: 16
    },
    textTitle: {
        fontSize: 16,
        marginTop: 10,
        marginBottom: 20,
        color: 'black',
        marginLeft: 5
    },
    iconColor: '#4493AA',
    picker: {
        height: 50,
        width: screen.width - 20,
    },
    textLabel: {
        color: '#bfbfbf',
        fontWeight: 'normal'
    },
    preTextInput: {
        width: '100%',
        height: 535,
        paddingLeft: 15,
        paddingRight: 15,
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        isOnline: state.getOnline.data,
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getCurrentUser: (data) => {
            dispatch(getCurrentUser(data))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);