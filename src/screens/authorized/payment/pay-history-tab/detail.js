import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, TextInput, ScrollView, Picker, ActivityIndicator } from 'react-native';
import { Text } from 'native-base';
import { connect } from 'react-redux';
import color from '../../../../ultis/styles/common-css';
import CustomTextInput from '../../../../components/text-input';
import BasicView from '../../../../ultis/styles/AppStyles';
import DatePicker from '../../../../components/date-picker';
import currency from 'currency.js';
import ToggleBox from '../toggle-box';
class DetailRevenuePerMonth extends Component {
    constructor(props) {
        super(props);


        this.state = {
            isLoading: false,
            totalMoney: 0,
            moneyPaid: 0,
            
        }

    }
    componentWillMount() {
       
    }
   
    render() {

        const { e, test } = this.props;

        if (test.length == 0) {
            return null
        }
        else {

            return (
                <View>
                    {
                        this.state.isLoading || this.props.isFeching && <ActivityIndicator style={{ alignSelf: 'center' }} />
                    }

                    <View style={{ height: 300, backgroundColor: color.colorBlue, justifyContent: 'space-around', padding: 10 }}>
                        <View style={[BasicView.horizontalView, {}]}>

                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Tên nhân viên:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.fullName}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Số điện thoại:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.phoneNum}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, { alignItems: 'center', }]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Tài khoản ngân hàng:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.bankName}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Số tài khoản:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.bankNum}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Số Module kích hoạt:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.activedDevices}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Ngày thanh toán:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>{this.props.lastUpdate}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={{ color: color.colorWhite, flex: 0.45, fontSize: 12 }}>Ghi chú:</Text>
                            <Text style={{ color: color.colorWhite, flex: 0.55, fontSize: 12 }}>Thưởng doanh số kích hoat tháng {this.props.month + 1}</Text>
                        </View>

                    </View>
                </View>
            )
        }

    }
}

const mapStateToProps = state => {
    return {
        employee: state.getCurrentUser.data,
        isFeching: state.getCurrentUser.isFeching
    }
}
const screen = Dimensions.get('window')
const styles = {
    container: {
        flex: 1,
    },
    text: {
        fontSize: 14,
        width: '20%',
        marginLeft: 5,
    },

    searchButton: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: '25%',
        backgroundColor: color.colorBlue,
        borderRadius: 4,
        alignSelf: 'center'
    },
    viewDatePicker: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 10,
        flexDirection: 'row',

    },
    viewPicker: {
        borderWidth: 0.5,
        borderRadius: 4,
        width: '50%',
        height: 40
    },
    coslapsibleContainer: {
        backgroundColor: '#fff',
        marginTop: 10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
}
export default connect(mapStateToProps)(DetailRevenuePerMonth);
