export default class PaymentController {
  _userId = '';
  _status = '';
  _fromDate = '';
  _toDate = '';
  _responseStatisticByStatus = [];
  _responseStatisticByOffset = [];
  getResponseStatisticByStatus() {
    return this._responseStatisticByStatus;
  };
  getResponseStatisticByOffset() {
    return this._responseStatisticByOffset;
  };
  getUserId() {
    return this._userId;
  };
  getStatus() {
    return this._status;
  };
  getFromDate() {
    return this._fromDate;
  };
  getToDate() {
    return this._toDate
  };

  setUserId(id) {
    this._userId = id;
  };
  setStatus(status) {
    this._status = status;
  };
  setFromDate(date) {
    this._fromDate = date;
  };
  setToDate(date) {
    this._toDate = date;
  };
  setResponseStatisticByStatus(response) {
    this._responseStatisticByStatus = response;
  };
  setResponseStatisticByOffset(response) {
    this._responseStatisticByOffset = this._responseStatisticByOffset.concat(response);
  };
  convertDateInput(value) {
    //console.log(new Date(value).getTime() - 24 * 60 * 60 * 1000);
    //return new Date(value).getTime() - 24 * 60 * 60 * 1000;
    let year = new Date(value).getFullYear();
    let months = new Date(value).getMonth();
    let dates = new Date(value).getDate();
    return new Date(year, months, dates,0,0,0).getTime()
  }
  _convertTo24h(value) {
    //console.log('Endtime', new Date(value).getTime() + 24 * 60 * 60 * 1000)
    let year = new Date(value).getFullYear();
    let months = new Date(value).getMonth();
    let dates = new Date(value).getDate();
    return new Date(year, months, dates, 23, 59,59,0).getTime()
  }
  _convertFormatDate(value) {
    let year = new Date(value).getFullYear();
    let month = new Date(value).getMonth() + 1;
    let date = new Date(value).getDate();
    return `${date}/${month}/${year}`
  }

  _totalValue(status) {
    var money = 0;
    if (status) {
      for (let i = 0; i < revenue.getResponseStatisticByStatus().length; i++) {
        money = money + revenue.getResponseStatisticByStatus()[i].moneyEmployeeRecent;

      }
      return money;
    }
    else {
      for (let i = 0; i < revenue.getResponseStatisticByOffset().length; i++) {
        money = money + revenue.getResponseStatisticByOffset()[i].moneyEmployeeRecent;

      }
      return money;
    }

  }
  _getActivationPaid(status) {
    var moneyPaid = 0;
    if (status) {
      for (let i = 0; i < this.getResponseStatisticByStatus().length; i++) {
        if (this.getResponseStatisticByStatus()[i].status == 0) {
          moneyPaid = moneyPaid + this.getResponseStatisticByStatus()[i].moneyEmployeeRecent;
        }
      }
      return moneyPaid;
    }
    else {
      for (let i = 0; i < this.getResponseStatisticByOffset().length; i++) {
        if (this.getResponseStatisticByOffset()[i].status == 0) {
          moneyPaid = moneyPaid + this.getResponseStatisticByOffset()[i].moneyEmployeeRecent;
        }
      }
      return moneyPaid;
    }
  }
  _getMinCreateDate(option) {
    var value = 0;
    if (this.getResponseStatisticByStatus().length === 1) {

      if (option == true) {
        value = this.getResponseStatisticByStatus()[0].createdDate
      }
      else value = this.getResponseStatisticByStatus()[0].createdDate
    }
    else {
      for (let i = 0; i < this.getResponseStatisticByStatus().length - 1; i++) {
        if (this.getResponseStatisticByStatus()[i].createdDate < this.getResponseStatisticByStatus()[i + 1].createdDate) {
          if (option == true) {
            value = this.getResponseStatisticByStatus()[i].createdDate
          }
          else value = this.getResponseStatisticByStatus()[i + 1].createdDate

        }
        else {
          if (option == true) {
            value = this.getResponseStatisticByStatus()[i + 1].createdDate
          }
          else value = this.getResponseStatisticByStatus()[i].createdDate
        }

      }
    }
    // Option = true =>>> get Min Value , false =>> get max value

    return value
  }
  compare(a, b) {
    const genreA = a.createdDate;
    const genreB = b.createdDate;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    //console.log(comparison)
    return comparison * -1;
  }
  async getStatisticByStatus(status, cb) {
    
    cb.setState({ isLoading: true })
   await fetch((`https://app-dot-casper-electric.appspot.com/api?action=statisticActivation`), {
      method: 'POST',
      body: JSON.stringify({
        startTime: this.convertDateInput(this.getFromDate()),
        endTime: this._convertTo24h(this.getToDate()),
        employeeId: cb.props.employeeId,
        status: status
      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {
        this.setResponseStatisticByStatus(responJSON.data.sort(this.compare));
        console.log('ABC', this.getResponseStatisticByStatus())
        cb.setState({
          isLoading: false, isFetched: true,
          totalMoney: this._totalValue(true),
          moneyPaid: this._getActivationPaid(true),
          activatedDevices: this.getResponseStatisticByStatus().length,
        })
      })
      .catch((error) => { console.log('getStatisticByStatus', error) })
      ;
  }
  async getActivationByOffset(offset, limit, employeeId, cb) {
    cb.setState({ isLoading: true })
    await fetch((`https://app-dot-casper-electric.appspot.com/api?action=getActivationLimit`), {

      method: 'POST',
      body: JSON.stringify({
        offset: offset,
        limit: limit,
        userId: employeeId,

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {
        //console.log("data", responJSON.data);
        if (responJSON.data.length !== 0) {
          cb.state.isLoadMore = true;
          this.setResponseStatisticByOffset(responJSON.data);
          let minDate = this._getMinCreateDate(true);
          let maxDate = this._getMinCreateDate(false);
          this.setFromDate(this._convertFormatDate(this.getResponseStatisticByOffset()[this.getResponseStatisticByOffset().length - 1].createdDate));
          this.setToDate(this._convertFormatDate(this.getResponseStatisticByOffset()[0].createdDate));
          cb.setState({ isLoading: false, isFetched: true, totalMoney: this._totalValue(false), fromDate: this.getFromDate(), toDate: this.getToDate(), moneyPaid: this._getActivationPaid(false), activatedDevices: this.getResponseStatisticByOffset().length });
          return null;
        }
        else {
          cb.state.isLoadMore = true;
          this.setFromDate(new Date().getTime());
          this.setToDate(new Date().getTime());
          cb.setState({ isLoading: false, isFetched: true, totalMoney: 0, activatedDevices: this.getResponseStatisticByOffset().length, noActivation: true });
          return null;
        }
      })
      .catch((error) => { console.log('getActivationByOffset', error) })
      ;
  }
   async getMoreActivationByOffset(offset, limit, employeeId, cb) {
    cb.setState({ isLoading: true })
    await fetch((`https://app-dot-casper-electric.appspot.com/api?action=getActivationLimit`), {

      method: 'POST',
      body: JSON.stringify({
        offset: offset,
        limit: limit,
        userId: employeeId,

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then(async (responJSON) => {
        //console.log("data", responJSON.data);
        if (responJSON.data.length !== 0) {
          cb.state.isLoadMore = true;


        }
        await this.setResponseStatisticByOffset(responJSON.data);
       // console.log('Fuck', this.getResponseStatisticByOffset())
        //console.log('Fuck', this.getResponseStatisticByOffset()[this.getResponseStatisticByOffset().length - 1].createdDate)
        let minDate = this._getMinCreateDate(true);
        let maxDate = this._getMinCreateDate(false);
        this.setFromDate(this._convertFormatDate(this.getResponseStatisticByOffset()[this.getResponseStatisticByOffset().length - 1].createdDate));
        this.setToDate(this._convertFormatDate(this.getResponseStatisticByOffset()[0].createdDate));
        cb.setState({ isLoading: false, isFetched: true, totalMoney: this._totalValue(false), fromDate: this.getFromDate(), toDate: this.getToDate(), moneyPaid: this._getActivationPaid(false), activatedDevices: this.getResponseStatisticByOffset().length });
      })
      .catch((error) => { console.log('getActivationByOffset', error) })
      ;
  }
}