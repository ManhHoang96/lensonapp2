import React, { Component } from 'react';

import { TabNavigator } from 'react-navigation';
import { View, Text, Platform } from 'react-native';
import { Tab, Tabs } from 'native-base';
import ActiveTab from './create-new-warranty';
import ActiveHistoryTab from './active-history-tab/';
import color from '../../../ultis/styles/common-css'

const styles = {
    activeTabStyle: {
        backgroundColor: color.colorBlue
    },
    activeTextStyle: {
        color: color.colorWhite,
        fontWeight: 'normal'
    },
    tabStyle: {
        borderTopWidth: 1,
        borderTopColor: color.colorBlue,
        backgroundColor: 'white'
    },
    textStyle: {
        color: color.colorBlue,
        fontWeight: 'normal'
    }
}

const ActiveTabScreen = TabNavigator({
    ActiveTab: { screen: ActiveTab },
    ActiveHistoryTab: { screen: ActiveHistoryTab }
}, {
        tabBarPosition: 'bottom',

        lazy: true,

        tabBarOptions: {
            ...Platform.select({
                ios: {
                    activeTintColor: color.colorWhite,
                    inactiveTintColor: color.colorBlue,
                    activeBackgroundColor: color.colorBlue,
                    indicatorStyle: {
                        borderTopColor: color.colorBlue,
                        borderTopWidth: 2,
                    },
                    showIcon: false,
                },
                android: {
                   style: {
                       backgroundColor: color.colorBlue
                   }
                },
               
            })



        }
    })
export default ActiveTabScreen;


