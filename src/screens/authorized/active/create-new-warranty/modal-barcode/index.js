//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Modal, Dimensions, TouchableOpacity, Image } from 'react-native';
import Camera from 'react-native-camera';
// create a component
export default class ModalBarCode extends Component {
     constructor(props) {
          super(props);
          this.state = {
               isScan: false,
               isShowBarCode: false,
          }
          iconOff = require('../../../../../../img/delete.png');
     }

     setShowBarCode(visiable) {
          this.setState({
               isShowBarCode: visiable,
               isScan: false,
          })
     }


     barcodeReceived(e) {
          if (e.data !== null && this.state.isScan === false) {
               this.props.getCodeIMEI(e.data);
               this.setState({
                    isScan: true,
                    isShowBarCode: false
               })
          }
     }

     render() {
          return (
               <Modal
                    style={styles.container}
                    transparent={true}
                    visible={this.state.isShowBarCode}
                    onRequestClose={() => this.setShowBarCode(false)}
               >
                    <View style={styles.content}>
                         <View style={styles.outsideContainer} />
                         <Camera
                              barcodeScannerEnabled={true}
                              onBarCodeRead={this.barcodeReceived.bind(this)}
                              ref={(cam) => {
                                   this.camera = cam;
                              }}
                              style={styles.preview}
                              aspect={Camera.constants.Aspect.fill}
                         />
                         <TouchableOpacity
                              style={styles.buttonOff}
                              onPress={() => this.setShowBarCode(false)}
                         >
                              <Image
                                   source={iconOff}
                                   style={styles.iconOff}
                                   resizeMode={'center'}
                              />
                         </TouchableOpacity>
                    </View>
               </Modal>
          );
     }
}
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
// define your styles
const styles = StyleSheet.create({
     container: {
          width: width,
          height: height,
          justifyContent: 'center',
          alignItems: 'center',
     },
     content: {
          width: width,
          height: height,
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center',
     },
     outsideContainer: {
          flex: 1,
          backgroundColor: '#000000',
          width: width,
          opacity: 0.6,
          position: 'absolute',
          height: height
     },
     preview: {
          width: 250,
          height: 250,
          alignSelf: 'center'
     },
     buttonOff: {
          position: 'absolute',
          bottom: 50,
          left: width / 2 - 30
     },
     iconOff: {
          width: 60,
          height: 60
     }
});
