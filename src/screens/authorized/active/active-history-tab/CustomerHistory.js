import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import color from '../../../../ultis/styles/common-css';
class CustomerHistory extends Component {
     constructor(props) {
          console.log('willMount')
          super(props);

          this.state = {
               aaa: ''
          }
     }
     async componentWillMount() {
          this.setState({ aaa: this.props.navigation.state.params.customer })
     }
     componentDidMount() {
          console.log('didmount', this.props.navigation.state.params.customer)
          console.log(this.state.aaa)
          //this._fetchHistory();
     }
     _renderItem = ({ item, index }) => (
          <View style={styles.contentView}>
               <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text}> Ngày kích hoạt: </Text>
                    <Text style={styles.text}>{this._convertFormatDate(item.createdDate)}</Text>
               </View>
               <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text}> Mã Model: </Text>
                    <Text style={styles.text}>{item.products[0].modelNumber}</Text>
               </View>
               <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text}> Tên Model: </Text>
                    <Text style={[styles.text, { width: 200 }]}>{item.products[0].nameModel}</Text>
               </View>


          </View>
     )
     _convertFormatDate(value) {
          let year = new Date(value).getFullYear();
          let month = new Date(value).getMonth() + 1;
          let date = new Date(value).getDate();
          return `${date}/${month}/${year}`
     }
     render() {
          console.log('Render', this.props.response)
          let data = this.props.response;
          if (data == undefined) return null;
          else {
               let header = 'Lịch sử kích hoạt của khách hàng '
               return (
                    <View style={{
                         height: '100%',
                         backgroundColor: 'white',
                         marginTop: 20
                    }}>

                         {
                              this.props.loadMenu && (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <ActivityIndicator />
                              </View>)
                         }
                         <Text style={styles.textHeader}>{header}: {this.props.customerId}</Text>
                         {
                              <FlatList
                                   data={data}
                                   extraData={this.state}
                                   keyExtractor={(item, index) => item.id}
                                   renderItem={this._renderItem}
                              />
                         }
                         <TouchableHighlight 
                              style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', width: 200, height: 50, backgroundColor: color.colorOrange, marginBottom: 30, borderRadius: 5 }}
                              onPress={() => this.props.closeMenu()}
                         >
                              <Text style={styles.text}> Đóng </Text>
                         </TouchableHighlight>
                    </View>
               );
          }
     }
}
const mapStateToProps = state => {
     return {
          currentUser: state.getCurrentUser.data
     }
}
const styles = {
     contentView: {
          height: 150,
          // width: "100%",
          flex: 1,
          backgroundColor: color.colorBlue,
          marginBottom: 20,
          justifyContent: 'space-around',
          padding: 10
     },
     textHeader: {
          fontSize: 16,
          fontWeight: 'bold',
          alignSelf: 'center',
          marginTop: 20,
          marginBottom: 20,
          marginLeft: 20
     },
     text: {
          color: color.colorWhite,
          //marginLeft: 10
     }
}
export default connect(mapStateToProps)(CustomerHistory);