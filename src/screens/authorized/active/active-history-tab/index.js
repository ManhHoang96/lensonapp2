import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, ScrollView, Text, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Drawer, Toast } from 'native-base';
import Menu from './CustomerHistory';
import color from '../../../../ultis/styles/common-css';
import CustomTextInput from '../../../../components/text-input';
import BasicView from '../../../../ultis/styles/AppStyles';
import DatePicker from '../../../../components/date-picker';
import Header from '../../../../components/header/Header';
import ListController from './controller';
import ButtonHotline from '../../../../components/button-hotline/ButtonHotline';
import ModalDetailActivation from './modal-detail-history';
class ActiveHistory extends Component {
    static navigationOptions = {
        tabBarLabel: 'Lịch sử kích hoạt'
    }
    constructor(props) {
        super(props);
        listController = new ListController();
        this.state = {
            isLoading: -1,
            listLoading: false,
            dataModel: {},
            titleCheckDate: '',
            loadMenu: false
        }
    }
    componentWillReceiveProps() {
        if(this.props.isOnline) {
            this._showToast('Không có kết nôi','danger','OK');
            return
        }
    }
    _showToast(text, type, buttonText, onClose) {
        Toast.show({
            text: text,
            position: 'top',
            buttonText: buttonText,
            type: type,
            onClose: onClose
        })
    }
    
    async componentDidMount() {

        let year = new Date().getFullYear();
        let month = new Date().getMonth();
        let date = new Date().getDate();
        listController.setStartDate(new Date(year, month - 1, date, 0, 0, 0).getTime());
        listController.setEndDate(new Date(year, month, date, 23, 59, 59).getTime());
        if (!this.props.isOnline) {
            this._showToast('Không có kết nối internet', 'danger', 'OK');
            return
        }
        await listController.getListActivation(listController, this)
    }
    //Sort data by key

    _getNextMonth() {
        let now = new Date();
        nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), now.getHours()).getTime();
        return nextMonth;
    }
    _renderFlatlistPanel(value, cb) {
        switch (value) {
            case -1: return null
            case 0: return (this._renderFlatList(cb))
        }
    }

    _renderDateMonthYear(value) {
        let date = new Date(value).getDate();
        let month = new Date(value).getMonth() + 1;
        let year = new Date(value).getFullYear();
        return `${date}/${month}/${year}`
    }
    _renderFlatList(cb) {
        if (listController.getResponseListActivation().length == 0) {
            return (<Text style={{ color: 'red', alignSelf: 'center', margin: 10 }}>Không có sản phẩm kích hoạt mới từ ngày {this._renderDateMonthYear(listController.getStartDate())} đến ngày {this._renderDateMonthYear(listController.getEndDate())}</Text>)
        }
        else {
            return (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ color: 'red', alignSelf: 'center', margin: 10 }}>{this.state.titleCheckDate}</Text>

                    <FlatList
                        data={listController.getResponseListActivation()}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this._renderItem}
                    />
                </View>

            )
        }
    }
    _renderItem = ({ item, index }) => (

        <TouchableOpacity
            onPress={() => {
                this.ModalDetailActivation._setShow(true);
                this.ModalDetailActivation.setState({ dataModel: item })
            }}
            style={styles.itemContainer}
        >
            <View style={[BasicView.horizontalView, {}]}>
                <Text style={[styles.titleItem, { fontWeight: 'bold' }]}>Khách hàng</Text>
                <TouchableOpacity onPress={async () => {
                    if(!this.props.isOnline) {
                        this._showToast('Không có internet','danger','OK');
                        return
                    }
                    this.drawer._root.open();
                    listController.setCustomerId(item.customerId)

                    await listController._fetchHistory(this);
                }} style={styles.valueItem}>
                    <Text
                        style={{ fontWeight: 'bold' }}

                    >{item.custommer.fullName}</Text>
                </TouchableOpacity>

            </View>
            <View style={[BasicView.horizontalView, {}]}>
                <Text style={[styles.titleItem, { fontWeight: 'bold' }]}>Số điện thoại</Text>
                <Text style={[styles.valueItem, { fontWeight: 'bold' }]}>{item.custommer.phoneNum}</Text>
            </View>
            <View style={[BasicView.horizontalView, {}]}>
                <Text style={styles.titleItem}>Ngày kích hoạt</Text>
                <Text style={styles.valueItem}>{this._renderDateMonthYear(item.createdDate)}</Text>
            </View>
            <View style={[BasicView.horizontalView, {}]}>
                <Text style={styles.titleItem}>Mã kích hoạt</Text>
                <Text style={styles.valueItem}>{item.guaranteeCode}</Text>
            </View>
        </TouchableOpacity>

    )
    _renderLastActivation(lastActivation, cb) {
        if (lastActivation.id == -1) {
            return null
        }
        else {
            return (
                <View>
                    <Text style={{ color: 'red', alignSelf: 'center', margin: 10 }}>Sản phẩm vừa kích hoạt cho khách</Text>

                    <TouchableOpacity
                        onPress={() => {
                            cb.ModalDetailActivation._setShow(true);
                            cb.ModalDetailActivation.setState({ dataModel: lastActivation })
                        }}
                        style={styles.itemContainer}
                    >
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={[styles.titleItem, { fontWeight: 'bold' }]}>Khách hàng</Text>
                            <View style={[BasicView.horizontalView, styles.valueItem]}>
                                <Text style={{ fontWeight: 'bold' }}>{lastActivation.customerId}</Text>
                            </View>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={styles.titleItem}>Ngày kích hoạt</Text>
                            <Text style={styles.valueItem}>{this._renderDateMonthYear(lastActivation.createDate)}</Text>
                        </View>
                        <View style={[BasicView.horizontalView, {}]}>
                            <Text style={styles.titleItem}>Mã kích hoạt</Text>

                        </View>
                    </TouchableOpacity>



                </View>
            )
        }

    }
    _startDay(value) {
        year = new Date(value).getFullYear();
        month = new Date(value).getMonth();
        date = new Date(value).getDate();
        return new Date(year, month, date, 0, 0, 0).getTime()
    }
    _endDay(value) {
        year = new Date(value).getFullYear();
        month = new Date(value).getMonth();
        date = new Date(value).getDate();
        return new Date(year, month, date, 23, 59, 59).getTime()

    }
    _checkDate() {
        if (!this.props.isOnline) {
            this._showToast('Không có kết nối internet', 'danger', 'OK');
            return
        }
        if (this._startDay(listController.getStartDate()) > this._endDay(listController.getEndDate())) {
            this.setState({ titleCheckDate: 'Ngày khởi đầu lớn hơn ngày kết thúc...' })
            return
        }
        else {
            listController.getListActivation(listController, this)
            return
        }
    }
    render() {
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={
                    <Menu
                        navigator={this.navigator}
                        navigation={this.props.navigation}
                        loadMenu={this.state.loadMenu}
                        response={listController.getResponse()}
                        customerId={listController.getCustomerId()}
                        closeMenu={() => { this.drawer._root.close() }}
                    />
                }
                side={'right'}
                openDrawerOffset={0.1}
            >
                <View style={styles.container}>
                    <Header
                        title={'Lịch sử kích hoạt'}
                        navigation={this.props.navigation}
                    />
                    <View style={[BasicView.horizontalView, { width: screen.width, justifyContent: 'space-around', marginTop: 20 }]}>
                        <DatePicker
                            getValue={(value) => { listController.setStartDate(this._startDay(value)) }}
                            showIcon={false}
                            style={styles.datepickerStyles}
                            title={'Từ ngày'}
                        />
                        <DatePicker
                            getValue={(value) => { listController.setEndDate(this._endDay(value)) }}
                            showIcon={false}
                            style={styles.datepickerStyles}
                            title={'Đến ngày'}
                        />
                    </View>
                    <TouchableOpacity style={styles.searchButton}
                        onPress={async () => { await this._checkDate() }}
                    >
                        <Text style={{ color: color.colorWhite }}>Tra cứu</Text>
                    </TouchableOpacity>
                    {

                        this._renderLastActivation(this.props.navigation.state.params.lastActivation, this)
                    }
                    {
                        this.state.listLoading && <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator />
                            <Text>Đang tìm kiếm, vui lòng chờ...</Text>
                        </View>
                    }
                    {
                        this._renderFlatlistPanel(this.state.isLoading, this)
                    }
                    <ButtonHotline
                        style={{ marginBottom: 10, marginTop: 10 }}
                    />
                    <ModalDetailActivation
                        ref={(ref) => this.ModalDetailActivation = ref}

                    />

                </View>
            </Drawer>
        );
    }
}

const screen = Dimensions.get('window')
const styles = {
    container: {
        flex: 1,


    },
    itemContainer: {
        height: screen.height * 0.15,
        width: screen.width - 20,
        alignSelf: 'center',
        paddingLeft: 10,
        justifyContent: 'space-around',
        backgroundColor: color.colorWhite,
        marginBottom: 10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,

    },
    titleItem: {
        flex: 0.35
    },
    valueItem: {
        flex: 0.65
    },
    textTitle: {
        marginTop: 10,
        borderTopWidth: 0.5,
        width: '100%',
        height: 50,
        textAlign: 'left',
        textAlignVertical: 'center',
        color: 'black',
        borderBottomWidth: 0.5,
        paddingLeft: 10
    },
    searchButton: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        backgroundColor: color.colorBlue,
        marginTop: 10,
        borderRadius: 4,
        alignSelf: 'center'
    },
    viewDatePicker: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 10,
        flexDirection: 'row'
    }
}
const mapStateToProps = state => {
    return {
        currentUser: state.getCurrentUser.data,
        isOnline: state.getOnline.data,
    }
}
export default connect(mapStateToProps)(ActiveHistory);