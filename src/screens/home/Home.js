import React, { Component } from 'react';
import { Drawer, Toast } from 'native-base';
import Menu from '../authorized/menu/Menu';
import { View, AsyncStorage, ActivityIndicator, Dimensions, Linking, Text, TouchableOpacity } from 'react-native';
import Header from '../../components/header/Header';
import Services from './app-services';
import * as apiConfig from '../../ultis/api/config';
import APPSTYLES from '../../ultis/styles/AppStyles';
import RootNav from '../../navigations/RootNav';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { getCurrentUser } from '../../redux/actions/get-current-user';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      fullName: '',
      uuid: '',
    }
    fullName = '';
    index = 0;

  }
  componentWillMount() {
    this.getUserInfo();
  }
  componentWillReceiveProps() {
    //func run khi isOnline thay doi, get gia tri preState isOnline
    if (this.props.isOnline === true && index === 1) { //state isonlie trước khi -> false có giá trị là true
      Toast.show({
        text: 'Không có kết nối!',
        position: 'bottom',
        buttonText: 'Okay',
        duration: 2000
      })
    }
    index = 1;
  }
  getUserInfo = async () => {
    try {
      const nameUser = await AsyncStorage.getItem('fullName');
      const sessionId = await AsyncStorage.getItem('key');
      const account = await AsyncStorage.getItem('account');
      this.setState({
        fullName: nameUser,
        uuid: sessionId
      });
      this.state.uuid = sessionId;
      //this.state.fullName = nameUser;
      if (this.props.isOnline === false) {
        Toast.show({
          text: 'Không có kết nối!',
          position: 'bottom',
          buttonText: 'Okay',
          duration: 2000
        })
      }
      this.props.getCurrentUser(account);
    } catch (error) {

    }
  }


  onPressLogout() {
    this.drawer._root.close()

    if (this.props.isOnline === true) {
      this.setState({ isLoading: true });
      fetch('https://app-dot-casper-electric.appspot.com/api?action=logout', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          sessionId: this.state.uuid,
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.logOut();
        })
        .catch((error) => {
          this.logOut()

        });
    } else {
      Toast.show({
        text: 'Không có kết nối!',
        position: 'bottom',
        buttonText: 'Okay',
        duration: 2000
      })
    }
  }

  logOut() {
    AsyncStorage.removeItem('key');
    AsyncStorage.removeItem('account');
    AsyncStorage.removeItem('fullName');
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Login' })
      ]
    })
    this.props.navigation.dispatch(resetAction)
  }


  render() {
    const { navigate } = this.props.navigation
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={
          <Menu
            navigator={this.navigator}
            fullName={this.state.fullName}
            onPressLogout={() => this.onPressLogout()}
            navigation={this.props.navigation}
            nextChangePass={()=>this.props.navigation.navigate('ChangePassword')}
          />
        }
        openDrawerOffset={0.3}
      >
        <View style={{ flex: 1 }}>

          <Header
            navigation={this.props.navigation}
            title={'Casper'}
            menu
            search
            onpressMenu={() => this.drawer._root.open()}
          />
          <View style={{ flex: 1 }}>

            <Services
              title={'KÍCH HOẠT'}
              logoName={'ACTIVE'}
              gotoServices={() => navigate('Active', { lastActivation: { id: -1 } })}
            />
            <Services
              title={'THÔNG TIN CHUNG'}
              logoName={'PRODUCT'}
              gotoServices={() => { Linking.openURL('http://casper-electric.com/') }}
            />
            <Services
              title={'THANH TOÁN'}
              logoName={'PAY'}
              gotoServices={() => { navigate('Payment') }}
            />
          </View>
          {
            (this.state.isLoading === true) ?
              <ActivityIndicator
                style={{ position: 'absolute', top: height / 2, left: width / 2 }}
              />
              : null
          }
        </View>
      </Drawer>
    );
  }
}
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const mapStateToProps = (state, ownProps) => {
  return {
    isOnline: state.getOnline.data,
    account: state.getCurrentUser.data
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getCurrentUser: (data) => {
      dispatch(getCurrentUser(data))
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);