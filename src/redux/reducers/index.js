import { combineReducers } from 'redux';
import getList from './customer';
import createUser from './create-user';
import getCurrentUser from './get-current-user';
import getOnline from './get-online';
import checkLoginBySessionId from './check-login-by-sessinId';
const rootReducer = combineReducers({  
     getList,  
     createUser, 
     getCurrentUser,
     getOnline,
     checkLoginBySessionId
});

export default rootReducer;