import React, { Component } from "react";
import { View, TouchableOpacity, Platform, Text } from 'react-native';
import { NavigationActions } from 'react-navigation'
import { Icon } from "native-base";
import styles from "./styles";

class HeaderComponent extends Component {

    _renderDefault() {
        let backAction = NavigationActions.back();
        return (
            <View
                style={[styles.header, (Platform.OS === 'ios' ? { marginTop: 15 } : null)]}
            >
                {
                    this.props.menu ?
                        <TouchableOpacity
                            onPress={this.props.onpressMenu}
                            style={styles.buttonLeft}
                        >
                            <Icon
                                name="ios-menu"
                                style={styles.iconLeftHeader}
                            />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            onPress={() => {
                                if (this.props.onPressBack !== undefined) {
                                    this.props.onPressBack();
                                } else {
                                    this.props.navigation.dispatch(backAction)
                                }
                            }}
                            style={styles.buttonLeft}
                        >
                            <Icon
                                name="arrow-back"
                                style={styles.iconLeftHeader}
                            />
                        </TouchableOpacity>
                }
                {
                    (this.props.title) ?
                        <Text style={styles.textTitle}>
                            {this.props.title}
                        </Text>
                        : null
                }
                {
                    this.props.share ? (
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('DrawerOpen')}
                            style={styles.buttonRight}
                        >
                            <Icon
                                name="share" style={styles.iconRightHeader}
                                onPress={() => this.props.navigation.navigate('DrawerOpen')}
                            />
                        </TouchableOpacity>
                    ) : null
                }
                {
                    this.props.option ? (
                        <TouchableOpacity
                            onPress={this.props.showNotifi}
                            style={styles.buttonRight}
                        >
                            <Icon
                                name="ios-options-outline" style={styles.iconRightHeader}
                                onPress={() => this.props.navigation.navigate('DrawerOpen')}
                            />
                        </TouchableOpacity>
                    ) : null
                }
            </View>

        );
    }

    render() {
        return (
            <View>
                {this._renderDefault()}
            </View>
        );
    }
};

export default HeaderComponent;
