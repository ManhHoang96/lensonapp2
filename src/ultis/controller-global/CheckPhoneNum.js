export default class CheckPhoneNum {
    phoneNum = '';
    setPhoneNum(phoneNum) {
        this.phoneNum = phoneNum;
    }
    getPhoneNum() {
        return this.phoneNum;
    }
    checkInvalidMobile(mobile) {
        if (!(mobile.match("[0-9]+"))) return false;
        if (mobile.startsWith("09") && mobile.length == 10) return true;
        if (mobile.startsWith("01") && mobile.length == 11) return true;
        if (mobile.startsWith("849") && mobile.length == 11) return true;
        if (mobile.startsWith("841") && mobile.length == 12) return true;
        return false;
    }
}